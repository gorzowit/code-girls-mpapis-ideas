class Idea < ApplicationRecord
  validates :title, presence: true, uniqueness: true, length: { minimum: 3 }
  validates :description, length: { minimum: 10 }

  belongs_to :user
end
