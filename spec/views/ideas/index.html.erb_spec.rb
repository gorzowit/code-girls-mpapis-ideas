require 'rails_helper'

RSpec.describe "ideas/index", type: :view do
  before(:each) do
    assign(:ideas, [
      Idea.create!(
        :title => "Title",
        :description => "MyDescription"
      ),
      Idea.create!(
        :title => "Title2",
        :description => "MyDescription"
      )
    ])
  end

  it "renders a list of ideas" do
    render
    assert_select "tr>td", :text => "Title", :count => 1
    assert_select "tr>td", :text => "Title2", :count => 1
    assert_select "tr>td", :text => "MyDescription", :count => 2
  end
end
